#!/usr/bin/python

#
# webApp class
#

import socket

port = 1234


# Clase donde se implementa la aplicación web
class WebApp:

    # Función para parsear la peticion
    def parse(self, request):
        print("Parse: Not parsing anything")
        return None

    # Funcion para procesar la solicitud
    def process(self, parsedRequest):
        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>Hola webApp!</h1></body></html>"

    # Inicializacion de la aplicacion web
    def __init__(self, hostname, port):
        # Create a TCP objet socket and bind it to a port
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print("Working at port:", port)
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received")
            request = recvSocket.recv(2048)
            print(request)
            parsedRequest = self.parse(request.decode('utf-8'))
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print("Answering back...")
            respuesta = ("HTTP/1.1 " + returnCode + " \r\n\r\n" + htmlAnswer + "\r\n")
            recvSocket.send(respuesta.encode('utf-8'))
            recvSocket.close()


if __name__ == "__main__":
    testWebApp = WebApp("localhost", port)
