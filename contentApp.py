#!/usr/bin/python

#
# contentApp class
#

import webApp

PAGE = """
<!DOCTYPE html>
<html lang="en">
    <body>
        {content}
    </body> 
</html>   
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <p><h1>Recurso no encontrado: {resource}.</h1></p>
            <p>Debes elegir entre el recurso /holaApp o /adiosApp</p>
    </body> 
</html>   
"""


class ContentApp(webApp.WebApp):
    contents = {'/': "<html><body><h1>Pagina Principal</h1></body></html>",
                '/holaApp': "<html><body><h1>Hola Mundo</h1></body></html>",
                '/adiosApp': "<html><body><h1>Adios Mundo Cruel</h1></body></html>"}

    def parse(self, request):
        parsed_request = request.split(' ',2)
        if len(parsed_request) > 1:
            return parsed_request[1]
        else:
            return "/"


    def process(self, resource):
        # Caso en el que el recurso sea conocido
        if resource in self.contents:
            content = self.contents[resource]
            pagina = PAGE.format(content=content)
            code = "200 OK"
        # Caso en el que el recurso no sea conocido
        else:
            pagina = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Page not found"
        return code, pagina


if __name__ == "__main__":
    webApp = ContentApp(hostname="localhost", port=1234)
